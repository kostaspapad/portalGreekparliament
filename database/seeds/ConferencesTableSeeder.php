<?php

use Illuminate\Database\Seeder;
use App\Models\Conference;

class ConferencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds. 
     * 
     * php artisan db:seed --class=ConferencesTableSeeder
     *
     * @return void
     */
    public function run()
    {
        DB::table('conferences')->delete();
        // echo "Truncated" . PHP_EOL;
        
        // If scraper_data has no data nothing will be inserted
        // $scraper_data_count = DB::table('scraper_data')->count();
        // if ($scraper_data_count == 0) {
        //     // Insert data to scraper data
        //     $this->call(ScraperDataTableSeeder::class);
        // }

        $conferences = DB::select('SELECT * FROM scraper_data');
        
        foreach($conferences as $c){
            DB::table('conferences')->insert([
                "id" => $c->id,
                "conference_date" => $c->conference_date,
                "conference_indicator" => $c->conference_indicator,
                "doc_location" => $c->doc_location,
                "doc_name" => urldecode($c->doc_name),
                "video_link" => $c->video_link,
                "session" => $c->session,
                "date_of_crawl" => $c->date_of_crawl,
                "pdf_loc" => $c->pdf_loc,
                "pdf_name" => $c->pdf_name,
                "time_period" => $c->time_period,
                "downloaded" => $c->downloaded
            ]);
        }
    }
}
