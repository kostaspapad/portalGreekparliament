<?php
/**
 * @package PortalGreekparliament
 * @author  Costas P <kpapadop92@gmail.com>
 * @license https://creativecommons.org/licenses/by-nc-nd/3.0/
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

/**
 * @package  portalGreekparliament
 * @author   Costas P <kpapadop92@gmail.com>
 * @license  https://creativecommons.org/licenses/by-nc-nd/3.0/
 */
class ScraperDataTableSeeder extends Seeder
{
    /**
     * Run seeds for scraper_data table. This tables must only be filled with
     * a seeder when the user builds the platform. 
     * 
     * NEVER RUN THE SEEDER ON A PRODUCTION DATABASE
     * php artisan db:seed --class=ScraperdataTableSeeder
     *
     * @return void
     */
    public function run()
    {
        if (!$this->askUser()) {
            echo "ABORTING!\n";
            exit;
        }

        DB::table('scraper_data')->truncate();
        echo "Truncated" . PHP_EOL;

        echo "Inserting new data" . PHP_EOL;

        $f = storage_path('app/public/scraper_data.csv');
        $data = $this->csvToArray($f);
        foreach ($data as $row) {
            DB::table('scraper_data')->insert(
                [
                "id" => $row[0],
                "conference_date" => $row[1],
                "time_period" => $row[2],
                "session" => $row[3],
                "conference_indicator" => $row[4],
                "video_link" => $row[5],
                "pdf_loc" => $row[6],
                "pdf_name" => $row[7],
                "doc_location" => $row[8],
                "doc_name" => $row[9],
                "date_of_crawl" => $row[10],
                "morning_conference" => $row[11],
                "noon_conference" => $row[12],
                "downloaded" => $row[13],
                ]
            );
        }
        
        echo 'Scraper data filled.' . PHP_EOL;
    }

    /** 
     * Ask yes or no question 
     * 
     * @return bool 
     */
    public function askUser()
    {
        echo "NEVER RUN THE scraper_data SEEDER ON A PRODUCTION DATABASE!!!".PHP_EOL;
        echo "Run only when you have a backup csv to update the data".PHP_EOL;
        echo "Conferences table will remain intact";
        echo "Are you sure you want to do this?  Type 'yes' to continue: ";
        $handle = fopen("php://stdin", "r");
        $line = fgets($handle);
        if (trim($line) != 'yes') {
            return false;
        }
        fclose($handle);
        return true;
    }

    /**
     * Read csv file and return an array
     * 
     * @param string $filename  Name of the file
     * @param string $delimiter What delimiter to use
     * 
     * @return array
     */ 
    public function csvToArray($filename = '', $delimiter = ',')
    {

        if (!file_exists($filename) || !is_readable($filename)) {
            echo 'Error in csvToArray'.PHP_EOL;
            return false;
        }

        $data = array();

        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                $data[] = $row;
            }
            
            fclose($handle);
        }
        
        return $data;
    }
}
