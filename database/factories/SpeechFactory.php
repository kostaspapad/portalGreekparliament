<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// $users = App\User::all()->get();
// dd($users);
$factory->define(App\Models\Speech::class, function (Faker $faker) {
    return [
        `speech_id` => $faker->rand(20000000000, 20200000000),
        `speech_conference_date` => $faker->date,
        `speaker_id` => $faker->uuid,
        `party_id` => $faker->uuid,
        `speech` => $faker->text,
        `created_at` => $faker->unixtime,
        `updated_at` => $faker->unixtime,
        `md5` => $faker->md5
    ];
});

