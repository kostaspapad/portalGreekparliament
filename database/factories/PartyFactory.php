<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Party::class, function (Faker $faker) {
    return [
        'party_id' => $faker->unique()->word,
        'fullname_el' => $faker->name,
        'fullname_en' => $faker->name,
        'image' => $faker->file($sourceDir = '/', $targetDir = '/tmp'),
        'url' => $faker->url
    ];
});
