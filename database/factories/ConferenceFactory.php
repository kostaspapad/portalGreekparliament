<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Conference::class, function (Faker $faker) {
    return [
        "id" => $faker->unique()->numberBetween($min = 1000000, $max = 2000000),
        "conference_date" => $faker->date,
        "conference_indicator" => $faker->word,
        "doc_location" => $faker->word,
        "doc_name" => $faker->word,
        "video_link" => $faker->word,
        "session" => $faker->word,
        "date_of_crawl" => $faker->date,
        "pdf_loc" => $faker->word,
        "pdf_name" => $faker->word,
        "time_period" => $faker->word,
        "downloaded" => $faker->numberBetween($min=0, $max=1)
    ];
});
