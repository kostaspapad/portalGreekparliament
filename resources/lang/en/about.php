<?php

return [
    'title1' => 'What is greekparliament.info?',
    'p1' => 'greekparliament.info is an attempt to extract data from the Hellenic Parliament plenary sessions and present them in a visible way. The page is currently in trial.',
    'title2' => 'What kind of data do you have?',
    'p2' => "Meetings that can be found on the official website of the House (www.hellenicparliament.gr) with the speeches separated by parliament member. We have all the talks from 1989 until today. Data on each Member (parliamentary course). Data on parties or parliamentary groups.However, due to the low cost of our server, our site presents speeches from '2010-01-11' until today, when we manage to get a server with more space for the database we will enter the entire volume of data.",
    'title3' => 'What is your stack?',
    'p3' => 'To extract the data (scraping and parsing) we use the python programming language along with some extra libraries and tools (antiword). This page is developed in PHP / Laravel / Vue.js. The database used is MySQL, with plans to use ElasticSearch for fulltext search functions in the future.',
    'title4' => 'Some sections of the page are not working properly / Some speeches or meetings are not available / Searches are too slow.',
    'p4' => 'The website is still under construction and unfortunately the problems cannot be resolved very quickly. The main problem is that our server is too small for the amount of data we produce and process. By moving to a larger server and more advanced technologies in the future we will be able to provide better services. However, any feedback on the difficulties you have with using the site is welcome.',
    'title5' => 'What are the new features you want to add?',
    'li1' => 'Links to videos of each meeting.',
    'li2' => 'More information for each speaker (Chairman, Minister, Minister, etc.).',
    'li3' => 'More information for each speech (Question, Reporting).',
    'li4' => 'Aggregated statistic for each sitting or parliamentary term per Member / Party. ',
    'li5' => 'Minutes of other meetings (city councils, other parliamentary committees).',
    'li6' => 'Ability to track words mentioned in the text and inform users by email or application.',
    'li7' => 'Search for county-based speakers they represent.',
    'li8' => 'Suggest a feature! Get in touch with us!',
    'title6' => 'How can I constribute to the project?',
    'li9' => 'The project is still in its infancy and is being developed by 3 people (who have also taken care of all the expenses), so any help is helpful. Need help with:',
    'li10' => 'Report bugs. Testing files and finding errors. Search for MPs information and manually add data.',
    'li11' => 'If you are a Data Scientist or Developer, any help / ideas are welcome. The same is true if you have a fast and reliable server available. Please contact us.',
    'li12' => 'But in fact the most important help you can offer is to spread our page. We are not advertising so we rely on word of mouth and good reputation.',
    'title7' => 'Can I get the data in raw form;',
    'p5' => 'Probably yes. Contact us by email '
];