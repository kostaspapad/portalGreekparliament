# Greekparliament.info

## What is greekparliament.info
Its a platform for the extraction and analysis of the Greek Parliament's plenary sessions.

Every day a scraper is running and downloads all the new conferences in raw text format, then a parser is extracting speaker names and speeches and inserts the structured data in to a database.

We have created a REST API for accesing the data but we haven't created documentation yet.

The structured data can be accesed from our portal.

## What you can do at greekparliament.info
Find speeches of MPs or political parties from the year 1989 until today.

Search the data using speaker names, party names, dates, keywords and even using full text search.

Find the polical party membership history of every politician.

Find what party spoke the most in every conference.

And more!

New features are going to be added when we find time to implement them.

We are a team of three people and we are doing this project for fun in our spare time.

## Want to constribute?

Send an email at greekparliamentinfo@gmail.com

## How To setup and run the project

See INSTALL.md for full installation instructions.